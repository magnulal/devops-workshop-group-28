package resources;

import dao.GroupChatDAO;
import data.GroupChat;
import data.Message;
import data.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * GroupChat resource exposed at "/groupchat" path
 */
@Path("/groupchat")
public class GroupChatResource {
    private GroupChatDAO groupChatDAO = new GroupChatDAO();

    /**
     * GET method to get one groupchat with specified groupChatId
     * @param groupChatId of the chat to GET
     * @return GroupChat
     */
    @GET
    @Path ("{groupChatId}")
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat getGroupChat(@PathParam("groupChatId") int groupChatId){
        GroupChat groupChat = groupChatDAO.getGroupChat(groupChatId);
        groupChat.setMessageList(groupChatDAO.getGroupChatMessages(groupChatId));
        groupChat.setUserList(groupChatDAO.getGroupChatUsers(groupChatId));

        return groupChat;
    }

    @GET
    @Path ("user/{userId}")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<GroupChat> getGroupConversations(@PathParam("userId") int userId) {
        return groupChatDAO.getGroupChatByUserId(userId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat postGroupChat(GroupChat groupChat) {
        return groupChatDAO.addGroupChat(groupChat);
    }

    @GET
    @Path("{groupChatId}/message")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<Message> getAllMessages(@PathParam("groupChatId") int groupChatId) {
        return groupChatDAO.getGroupChatMessages(groupChatId);
    }

    @POST
    @Path("{groupChatId}/message")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces (MediaType.APPLICATION_JSON)
    public Message postMessage(@PathParam("groupChatId") int groupChatId, Message message) {
        return groupChatDAO.addMessage(groupChatId, message);
    }


}
