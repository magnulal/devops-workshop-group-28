package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Calculator resource exposed at '/calculator' path
 */
@Path("/calculator")
public class CalculatorResource {

    /**
     * Method handling HTTP POST requests. The calculated answer to the expression will be sent to the client as
     * plain/text.
     * @param expression the expression to be solved as plain/text.
     * @return solution to the expression as plain/text or -1 on error
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String calculate(String expression){
        // Removes all whitespaces
        String expressionTrimmed = expression.replaceAll("\\s+","");

        String result = "Illegal input";

        /*
         * .matches use regex expression to decide if a String matches a given pattern.
         * [0-9]+[+][0-9]+ explained:
         * [0-9]+: a number from 0-9 one or more times. For example 1, 12 and 123.
         * [+]: the operator + one time.
         * The total regex expression accepts for example:
         * 12+34,
         * 1+2,
         * 10000+1000
         */

        if(expressionTrimmed.matches("[0-9]+([+][0-9]+)+")) result = sum(expressionTrimmed);
        else if(expressionTrimmed.matches("[0-9]+([-][0-9]+)+")) result = subtraction(expressionTrimmed);
        else if(expressionTrimmed.matches("[0-9]+([*][0-9]+)+")) result = multiplication(expressionTrimmed);
        else if(expressionTrimmed.matches("[0-9]+([/][0-9]+)+")) result = division(expressionTrimmed);

        return String.valueOf(result);
    }

    /**
     * Method used to calculate a sum expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public String sum(String expression){
        String[] split = expression.split("[+]");

        int sum = 0;

        for (int i = 0; i < split.length; i++) {
            sum += Integer.parseInt(split[i]);
        }

        return String.valueOf(sum);
    }

    /**
     * Method used to calculate a subtraction expression.
     * @param expression the expression to be calculated as a String
     * @return the answer as an int
     */
    public String subtraction(String expression){
        String[] split = expression.split("[-]");

        int differance = Integer.parseInt(split[0]);

        for (int i = 1; i < split.length; i++) {
            differance -= Integer.parseInt(split[i]);
        }

        return String.valueOf(differance);
    }

    public String multiplication(String equation) {
        String[] split = equation.split("[*]");

        int product = 1;

        for (int i = 0; i < split.length; i++) {
            product *= Integer.parseInt(split[i]);
        }

        return String.valueOf(product);
    }

    public String division(String equation) throws ArithmeticException {
        String[] split = equation.split("[/]");

        int differance = Integer.parseInt(split[0]);

        for (int i = 1; i < split.length; i++) {
            if (Integer.parseInt(split[i]) == 0) {
                return "Can not divide by zero";
            }
            differance /= Integer.parseInt(split[i]);
        }

        return String.valueOf(differance);

    }
}