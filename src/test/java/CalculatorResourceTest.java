import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100 + 300 + 1";
        assertEquals("401", calculatorResource.calculate(expression));

        expression = " 300 - 99";
        assertEquals("201", calculatorResource.calculate(expression));

        expression = " 3 * 10";
        assertEquals("30", calculatorResource.calculate(expression));

        expression = " 300 / 10";
        assertEquals("30", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+10";
        assertEquals("410", calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals("399", calculatorResource.sum(expression));

        expression = "300+99+1+1";
        assertEquals("401", calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100-10";
        assertEquals("889", calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals("18", calculatorResource.subtraction(expression));

        expression = "20-22";
        assertEquals("-2", calculatorResource.subtraction(expression));
    }


    @Test
    public void testDivision() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "300/10/10";
        assertEquals("3", calculatorResource.division(expression));

        expression = "20/2";
        assertEquals("10", calculatorResource.division(expression));
    }

    @Test
    public void testDivisionWith0() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "300/0";

        assertEquals("Can not divide by zero", calculatorResource.division(expression));
    }

    @Test
    public void testMultiplication() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "3*10*5";
        assertEquals("150", calculatorResource.multiplication(expression));

        expression = "20*2";
        assertEquals("40", calculatorResource.multiplication(expression));
    }
}
